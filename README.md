# Alienet

Mostly uncommon Linux Network configuration & network labs tests

 - [6rd](../6rd):  A complete 6rd router gateway configuration based on the new nftable implementation. This conf is use for a french ISP (Free/Iliad SAS) 

 - [gpon](../gpon): Proof of concept of 10G-EPON network End-End for a french ISP (Free/Iliad) without the given material (ONU + Router) for a Multipoint Fiber Connection
